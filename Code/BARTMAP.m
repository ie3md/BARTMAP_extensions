%% """ BARTMAP """
% 
% __author__ = 'Islam Elnabarawy'
% __email__ = 'ie3md@mst.edu'
% 
% This is a MATLAB implementation of the "BARTMAP" network.
% 
% References:
% [1] R. Xu and D. C. Wunsch II, "BARTMAP: A viable structure for biclustering," 
% Neural Networks, vol. 24, no. 7, pp. 709�716, 2011.

% BARTMAP Class
classdef BARTMAP
    
    %% properties
    properties
        ARTa
        ARTa_settings
        ARTa_max_epochs
        
        ARTb
        ARTb_settings
        ARTb_max_epochs
        
        correlation_threshold
        correlation_function
        step_size
    end
    
    %% methods
    methods
        %% Assign property values from within the class constructor
        function obj = BARTMAP(settings)
            obj.ARTa = feval(settings.ARTa.class, settings.ARTa.settings);
            obj.ARTa_settings = settings.ARTa.settings;
            obj.ARTa_max_epochs = settings.ARTa.max_epochs;

            obj.ARTb = feval(settings.ARTb.class, settings.ARTb.settings);
            obj.ARTb_settings = settings.ARTb.settings;
            obj.ARTb_max_epochs = settings.ARTb.max_epochs;
            
            obj.correlation_threshold = settings.correlation_threshold;
            obj.correlation_function = settings.correlation_function;
            obj.step_size = settings.step_size;
        end 
        
        %% Train
        function obj = train(obj, data)
        % Run the Biclustering ARTMAP algorithm on the given data

            %% read dataset attributes
            num_users = size(data, 2);

            %% normalize the data to the range [0, 1]
            data = BARTMAP.minmax_norm(data);

            %% call the Fuzzy ART algorithm for ART_b
            obj.ARTb = obj.ARTb.train(data, obj.ARTb_max_epochs);

            %% perform the biclustering using the ART_a module

            num_user_clusters = 0;
            user_clusters = zeros(num_users, 1);

            data = data';   % transpose the data to look at the other dimension

            t = tic;
            h = waitbar(0.0, 'Processing user Clusters...');
            %% present the user data one by one to ART_a
            for ix=1:num_users
                if toc(t) > 10
                    t = tic;
                    waitbar(ix/num_users, h, sprintf(...
                        'Processing user Clusters (%d/%d)...', ix, num_users));
                end

                %% initialize ARTa vigilance parameter every loop
                obj.ARTa.rho = obj.ARTa_settings.rho;

                %% Fuzzy ARTMAP training loop per user
                while true
                    %% present user data to ART_a module
                    obj.ARTa = obj.ARTa.train(data(ix,:), obj.ARTa_max_epochs);

                    user_clusters(ix) = obj.ARTa.labels;

                    %% check if the user was assigned to an existing or new cluster
                    if num_user_clusters == obj.ARTa.num_clusters 
                        %% the user was assigned to an existing cluster
                        % compute the average correlations between this user and
                        % each user/item bicluster
                        correlations = zeros(1, obj.ARTb.num_clusters);
                        user_ix = find(user_clusters == obj.ARTa.labels);
                        user_ix = user_ix(user_ix ~= ix);
                        for jx = 1:obj.ARTb.num_clusters
                            item_ix = find(obj.ARTb.labels == jx);
                            bicluster = data(user_ix, item_ix);
                            user_data = data(ix, item_ix);
                            correlations(jx) = BARTMAP.biclusterCorr(bicluster, ...
                                user_data, obj.correlation_function);
                        end

                        %% check the correlation threshold
                        if ~isempty(find(correlations > obj.correlation_threshold, 1)) ...
                                || obj.ARTa.rho == 1
                            % allow the weight update to be saved and go to next user
                            break;
                        else
                            % reset the weights, increase the vigilance threshold and try again
                            obj.ARTa = obj.ARTa.reset_weights();
                            obj.ARTa.rho = obj.ARTa.rho + obj.step_size;
                            if obj.ARTa.rho > 1
                                obj.ARTa.rho = 1;
                            end
                        end

                    else
                        %% new cluster created; always allow new clusters
                        num_user_clusters = obj.ARTa.num_clusters;
                        break;
                    end
                end
            end

            close(h);

            %% save all user labels into ARTa
            obj.ARTa.labels = user_clusters;

        end
        
        %% Evaluate
        function [rating] = eval(obj, data, user_id, item_id)
            % Apply the BARTMAP model to estimate the user-item rating

            %% extract the model variables

            user_clusters = obj.ARTa.labels;
            item_clusters = obj.ARTb.labels;

            %% normalize the data to the range [0, 1]

            data = BARTMAP.minmax_norm(data)';

            %% handle multiple user and item indices
            rating = zeros(size(user_id));
            t = tic;
            h = waitbar(0.0, 'Evaluating user clusters...');
            for ix = 1:numel(user_id)
                if toc(t) > 10
                    t = tic;
                    waitbar(ix/numel(user_id), h, sprintf(...
                        'Evaluating user clusters (%d/%d)...', ix, numel(user_id)));
                end

                %% extract loop variables
                uix = user_id(ix);
                iix = item_id(ix);

                %% find the bicluster and separate the user's data
                user_ix = find(user_clusters == user_clusters(uix));
                if numel(user_ix) > 1 
                    % guard against single-user clusters
                    user_ix = user_ix(user_ix ~= uix);
                end
                item_ix = find(item_clusters == item_clusters(iix));
                bicluster = data(user_ix, item_ix);
                user_data = data(uix, item_ix);
                item_ix = find(item_ix == iix);

                %% predict item rating based on user cluster's mean
                rix = BARTMAP.predictRating(bicluster, user_data, item_ix, ...
                    obj.correlation_function);

                %% map the rating back to the correct range
                if rix < 0
                    rix = 0;
                elseif rix > 1
                    rix = 1;
                end
                rix = rix * 5;

                %% assign the output
                rating(ix) = rix;

            end
            
            close(h);

        end
    end
    
    methods(Static)
        %% coMean calculation
        function [Xmean, Ymean, coMask, coCount] = coMean(X, Y)
            %COMEAN Find the means of X and Y only for where X and Y are both nonzero

            coMask = X > 0 & Y > 0;
            Xmean = mean(X(coMask));
            Ymean = mean(Y(coMask));
            coCount = sum(coMask);
        end

        %% Input normalization
        function output = minmax_norm(input, range)
            %MINMAX_NORM Normalize the input to map it to range, [0, 1] by default

            %% set default argument values
            if nargin < 2
                range = [0, 1];
            end

            %% find the range of input values
            x = input(:);
            xmin = min(x);
            xmax = max(x);

            ymin = range(1);
            ymax = range(2);

            output = (ymax - ymin) * (input - xmin) / (xmax - xmin) + ymin;

        end
        
        %% Bicluster correlation
        function [result, bicorr] = biclusterCorr(bicluster, user_data, correlation_function)
            %BICLUSTERCORR Compute correlation between a user and a user/item bicluster

            %% preallocate correlation vector
            num_users = size(bicluster, 1);
            bicorr = zeros(1, num_users);

            %% compute the correlation for each pair of users
            for ix = 1:num_users
                %% calculate the co-mean of this pair of users
                [u1_mean, u2_mean, coMask] = BARTMAP.coMean(user_data, bicluster(ix, :));
                %% compute the terms for all the item values
                terms1 = user_data(coMask) - u1_mean;
                terms2 = bicluster(ix, coMask) - u2_mean;
                %% compute the sums to find the user-pair correlation
                numerator = sum(terms1 .* terms2);
                root1 = sqrt(sum(terms1 .* terms1));
                root2 = sqrt(sum(terms2 .* terms2));
                if root1 == 0 || root2 == 0
                    r = 0;
                else
                    r = numerator / (root1 * root2);
                end
                
                %% calculate all the correlation function values
                r2 = r .^ 2;
                n = sum(coMask);
                if n < 2
                    % protect against sqrt of a negative number
                    t = 0;
                else
                    if r2 == 1
                        % protect against division by zero
                        r2 = r2 - 1e-6;
                    end
                    t = r .* sqrt((n-2)./(1-r2));
                end
                
                %% return the appropriate correlation function value
                switch correlation_function
                    case { 'r', 'pearson' }
                        bicorr(ix) = r;
                    case { 'abs-r', 'abs-pearson' }
                        bicorr(ix) = abs(r);
                    case { 'r2', 'cod', 'CoD' } % coefficient of determination (r^2)
                        bicorr(ix) = r2;
                    case { 't', 'student' } % t statistic correlation
                        bicorr(ix) = t;
                    case { 'abs-t', 'abs-student' } % absolute t-statistic value
                        bicorr(ix) = abs(t);
                end
            end

            %% compute the final correlation coefficient for bicluster
            result = mean(bicorr);

        end
        
        %% Predict rating
        function [result] = predictRating(bicluster, user_data, item_ix, correlation_function)
            %PREDICTRATING Predict user's rating for item based on bicluster data

            %% compute the correlation for each pair of users
            [~, bicorr] = BARTMAP.biclusterCorr(bicluster, user_data, correlation_function);

            %% calculate the user's average rating
            ix = find(user_data > 0);
            ix = ix(ix ~= item_ix);
            if ~isempty(ix)
                % guard against users with no ratings in cluster
                user_mean = mean(user_data(ix));
            else
                user_mean = 0.5;
            end

            %% calculate the item's weighted average rating
            user_ix = bicluster(:, item_ix) > 0;
            item_data = bicluster(user_ix, item_ix);
            corr_data = bicorr(user_ix)';
            if ~isempty(item_data) && nnz(corr_data) > 0
                % guard against items with no ratings in cluster and zero correlations
                item_mean = mean(item_data);
                result = sum(corr_data .* (item_data - item_mean)) / sum(abs(corr_data));
            else
                result = 0;
            end

            %% compute the final prediction for this item
            result = user_mean + result;

        end
    end
end
