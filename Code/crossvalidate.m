%% clean up
fclose all; close all; clear; clc;

%% set the algorithm parameters

params = struct();

% ARTb module used for item clustering
params.ARTb = struct();
params.ARTb.class = 'FuzzyART';
params.ARTb.settings = struct( ...
    'rho', 0.15, ...
    'alpha', 0.001, ...
    'gamma', 1.0 ...
);
params.ARTb.max_epochs = 100;

params.correlation_threshold = 0.15;
params.correlation_function = 'pearson';

% ARTa module used for user clustering
params.ARTa = struct();
params.ARTa.class = 'FuzzyART';
params.ARTa.settings = struct( ...
    'rho', 0.35, ...
    'alpha', 0.001, ...
    'gamma', 1.0 ...
);
params.ARTa.max_epochs = 100;

params.step_size = 0.05;

%% load cross-validation folds

load data/ml-100k-folds.mat;

%% try all the different correlation functions

correlation_functions = { 'r', 'abs-r', 'cod', 't', 'abs-t' };

for fx = correlation_functions
    fn = fx{1};
    params.correlation_function = fx{1};
    fprintf('Correlation function: %s\n', fn);

%% open log file

f_log = fopen(sprintf('cv-%s.log', fn), 'w');

%% loop over the different cross-validation folds
results = cell(1, numel(dataset));
for ix = 1:numel(dataset)
    %% extract the training data
    dx = dataset{ix};
    data = dx.train.matrix;
    
    %% call the BARTMAP algorithm using the training data
    t = tic;
    results{ix} = struct();

    bartmap = BARTMAP(params);
    bartmap = bartmap.train(data);

    results{ix}.train_time = toc(t);
    results{ix}.model = bartmap;

    %% extract the test data
    userIds = dx.test.userIds;
    itemIds = dx.test.itemIds;
    ratings = dx.test.ratings;

    %% calculate the training error
    t = tic;
    results{ix}.predicted = bartmap.eval(data, userIds, itemIds);
    results{ix}.test_time = toc(t);
    results{ix}.errors = results{ix}.predicted - ratings;
    
    %% calculate the performance measures
    results{ix}.mean_abs_err = mean(abs(results{ix}.errors));
    results{ix}.mean_sq_err = mean(results{ix}.errors .^ 2);
    results{ix}.root_mean_sq_err = sqrt(results{ix}.mean_sq_err);

    %% write output to log file
    fprintf(f_log, '%d: MAE = %.2f, MSE = %.2f, RMSE = %.2f, T1 = %.2f, T2 = %.2f\n', ...
        ix, results{ix}.mean_abs_err, results{ix}.mean_sq_err, ...
        results{ix}.root_mean_sq_err, results{ix}.train_time, ...
        results{ix}.test_time);

    %% write output to console too
    fprintf('%d: MAE = %.2f, MSE = %.2f, RMSE = %.2f, T1 = %.2f, T2 = %.2f\n', ...
        ix, results{ix}.mean_abs_err, results{ix}.mean_sq_err, ...
        results{ix}.root_mean_sq_err, results{ix}.train_time, ...
        results{ix}.test_time);

    %% plot the absolute error histogram
    figure(ix); hist(abs(results{ix}.errors), 5);
    title(sprintf('Mean Absolute Error = %.2f', results{ix}.mean_abs_err));
    xlabel('Rating Difference'); 
    ylabel('Number of ratings');
    xlim([0, 5]);
    drawnow;
    
end

%% close log file
fclose(f_log);

end