%% """ Fuzzy ART """
% 
% __author__ = 'Islam Elnabarawy'
% __email__ = 'ie3md@mst.edu'
% 
% This is a MATLAB implementation of the original "Fuzzy ART" network.
% 
% References:
% [1] G. Carpenter, S. Grossberg, and D. Rosen, "Fuzzy ART: Fast 
% stable learning and categorization of analog patterns by an adaptive 
% resonance system," Neural networks, vol. 4, no. 6, pp. 759�771, 1991.

% Fuzzy ART Class
classdef FuzzyART
    %FUZZYART Fuzzy ART unsupervised learning neural network

    %% properties
    properties
        rho             % vigilance parameter: [0, 1]
        alpha           % alpha parameter 
        gamma           % learning rate prameter
        num_clusters
        labels
        W
        iterations
        W_reset
    end
    
    %% methods
    methods
        %% Assign property values from within the class constructor
        function obj = FuzzyART(settings)     
            obj.rho = settings.rho;
            obj.alpha = settings.alpha;
            obj.gamma = settings.gamma;
            obj.num_clusters = 0;
            obj.W = [];
            obj.iterations = 0;
            obj.labels = [];
            obj.W_reset = [];
        end 
        
        %% Train
        function obj = train(obj, data, max_epochs)
            %% initialize variables

            [num_samples, num_features] = size(data); 
            num_inputs = num_features * 2;

            obj.W_reset = obj.W;
            
            if isempty(obj.W)
                obj.num_clusters = 0;
                obj.W = ones(obj.num_clusters+1, num_inputs);
            end

            W_old = zeros(obj.num_clusters, num_inputs);

            obj.labels = zeros(num_samples, 1);

            obj.iterations = 1;

            %% complement-code the data

            coded_data = zeros(num_samples, num_inputs);
            for ix = 1:num_features
                coded_data(:,2*ix-1) = data(:,ix);
                coded_data(:,2*ix) = 1-data(:,ix);
            end

            %% repeat the learning until either convergence or max_epochs
            while ~isequal(W_old, obj.W) && obj.iterations < max_epochs
                W_old = obj.W;
                %% present the input patters to the Fuzzy ART module
                for ix = 1:num_samples
                    pattern = coded_data(ix,:);
                    %% calculate the category choice values
                    matches = zeros(1, obj.num_clusters+1);
                    min_norms = zeros(1, obj.num_clusters+1); % we will need those again
                    for jx = 1:obj.num_clusters+1
                        min_norms(jx) = norm(min(pattern, obj.W(jx,:)), 1);
                        matches(jx) = min_norms(jx) ./ (obj.alpha + norm(obj.W(jx,:), 1));
                    end
                    %% pick the winning category
                    % move pattern norm to right side of inequality to speed things up
                    vigilance_test = obj.rho * norm(pattern, 1); 
                    while true
                        %% winner-take-all selection
                        % we only care about the index of the winning neuron
                        [~, jx] = max(matches);

                        %% vigilance test
                        if min_norms(jx) >= vigilance_test
                            % the winning category passed the vigilance test
                            obj.labels(ix) = jx;
                            break;
                        else
                            % shut off this category from further testing
                            matches(jx) = 0;
                        end
                    end
                    %% update the weight of the winning neuron
                    obj.W(jx,:) = obj.gamma * min(pattern, obj.W(jx,:)) + (1-obj.gamma) * obj.W(jx,:);
                    %% check if the uncommitted node was the winner
                    if jx > obj.num_clusters
                        % add a new uncommitted node
                        obj.num_clusters = obj.num_clusters + 1;
                        obj.W = [ obj.W ; ones(1, num_inputs) ];
                    end
                end
                obj.iterations = obj.iterations + 1;
            end            
        end
        
        %% Reset weights
        function obj = reset_weights(obj)
            obj.W = obj.W_reset;
            obj.num_clusters = size(obj.W, 1) - 1;
        end
    end
    
end

